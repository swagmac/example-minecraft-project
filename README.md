# example-minecraft-project

Your TODO's:
-
	1. fork this repository an be sure to set it to private
	2. configure IntelliJ correctly
	3. set .gitignore file for serverconfigs

IntelliJ configuration
-
    - Import your project from VCS
    - In Project Structure:
        - Set JavaSDK
        - Add Java library (_project root_/server/spigot.x.xx.jar)
        - Add artifacts (JAR -> From module with dependencies -> OK)
        - change output path to (_project root_/server/plugins)
        - tick "Include in project build"
        - add your _plugin.yml_ to the jar build
    - Hint: Configure Run Configuration as desired     
        
Project configuration
-
    - mark directory as Excluded
        - .idea
        - out
        
        
Links:

https://www.spigotmc.org/

https://hub.spigotmc.org/javadocs/bukkit/overview-summary.html