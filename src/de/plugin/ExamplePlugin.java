package de.plugin;

import de.plugin.listeners.CowHitListener;
import org.bukkit.Bukkit;
import org.bukkit.command.Command;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;
import org.bukkit.plugin.java.JavaPlugin;
import org.bukkit.scheduler.BukkitScheduler;

public class ExamplePlugin extends JavaPlugin {

    @Override
    public void onEnable() {
        this.getServer().broadcastMessage("plugin ... running");
        new CowHitListener(this);

        BukkitScheduler scheduler = Bukkit.getServer().getScheduler();
        scheduler.scheduleSyncRepeatingTask(this, new Runnable() {
            @Override
            public void run() {
                //do something
            }
        }, 0, 20L);
    }

    @Override
    public boolean onCommand(CommandSender sender, Command command, String label, String[] args) {
        if (sender instanceof Player && label.equalsIgnoreCase("example")) {
            sender.getServer().broadcastMessage("You issued an example command");
            return true;
        }
        return false;
    }
}
