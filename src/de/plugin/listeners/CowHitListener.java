package de.plugin.listeners;

import de.plugin.ExamplePlugin;
import org.bukkit.entity.EntityType;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.entity.EntityDamageByEntityEvent;

public class CowHitListener implements Listener {
    public CowHitListener(ExamplePlugin plugin){
        plugin.getServer().getPluginManager().registerEvents(this, plugin);
    }

    @EventHandler
    public void onCowHit(EntityDamageByEntityEvent event) {
        if (event.getEntity().getType() == EntityType.COW) {
            event.getEntity().setFireTicks(1000);
        }
    }
}
